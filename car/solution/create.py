from client import MongoLsv
bd_lsv = "car_lsv"
mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name = bd_lsv))


mongo_lsv.create_new_record_in_collection(
        db_name=bd_lsv,
        collection="partes_bicicleta",
        record={"name": "volante", "marca": "Pajarito"},
)
mongo_lsv.create_new_record_in_collection(
        db_name=bd_lsv,
        collection="bicicleta",
        record={"name":"numero1","marca":"Universe","frenos":"BMX"})

print(mongo_lsv.get_records_from_collection(db_name = bd_lsv, collection="bicicleta"))
print(mongo_lsv.get_records_from_collection(db_name = bd_lsv, collection="partes_bicicleta"))