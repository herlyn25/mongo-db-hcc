from bson import ObjectId
from client import MongoLsv

bd_lsv = "car_lsv"
mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name = bd_lsv))

print(mongo_lsv.update_record_in_collection(
        db_name=bd_lsv,
        collection="bicicleta",
        record_query={"_id": ObjectId("62ad60c46679b3dfc1a9d84b")},
        record_new_value={"name": "numero2"},
    )
)
print(mongo_lsv.update_record_in_collection(
        db_name=bd_lsv,
        collection="partes_bicicleta",
        record_query={"_id": ObjectId("62ad60c46679b3dfc1a9d84a")},
        record_new_value={"name": "cadena"},
    )
)