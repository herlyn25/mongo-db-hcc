from client import MongoLsv
bd_lsv = "car_lsv"
mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name = bd_lsv))
print(mongo_lsv.get_records_from_collection(db_name = bd_lsv, collection="bicicleta"))
print(mongo_lsv.get_records_from_collection(db_name = bd_lsv, collection="partes_bicicleta"))

print(mongo_lsv.delete_record_in_collection(db_name=bd_lsv,collection="bicicleta",record_id="62ad56ac7c946e5e33478fc9"))
print(mongo_lsv.delete_record_in_collection(db_name=bd_lsv,collection="partes_bicicleta", record_id="62ad60c46679b3dfc1a9d84a"))